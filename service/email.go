package service

import (
	"crypto/tls"
	"fmt"
	"log"
	"net/mail"
	"net/smtp"
	"online-tabu-server/config"
	"online-tabu-server/models"
)

func SendInvitationEmail(stageId string, player *models.Player) {

	if "" == player.Email {
		return
	}

	// smtp server configuration.
	smtpHost := config.SmtpServer
	smtpPort := config.SmtpPort
	smtpHostWithPort := config.SmtpServer + ":" + smtpPort
	password := config.SmtpPassword

	from := mail.Address{"", "online-tabu@sontim.de"}
	to := mail.Address{"", player.Email}
	subj := "Einladung zum Online Tabu"
	body := "Dies ist eine Einladung zum Online TABU. Folge diesem Link: https://corona-tabu.de und gib folgenden Code ein: " + player.LoginCode

	// Setup headers
	headers := make(map[string]string)
	headers["From"] = from.String()
	headers["To"] = to.String()
	headers["Subject"] = subj

	// Setup message
	message := ""
	for k, v := range headers {
		message += fmt.Sprintf("%s: %s\r\n", k, v)
	}
	message += "\r\n" + body
	// Authentication.
	auth := smtp.PlainAuth("", config.SmtpUser, password, smtpHost)

	//// Sending email.
	//err := smtp.SendMail(smtpHost+":"+smtpPort, auth, from, to, message)
	//if err != nil {
	//	fmt.Println(err)
	//	return
	//}
	// TLS config
	tlsconfig := &tls.Config{
		InsecureSkipVerify: true,
		ServerName:         smtpHost,
	}

	// Here is the key, you need to call tls.Dial instead of smtp.Dial
	// for smtp servers running on 465 that require an ssl connection
	// from the very beginning (no starttls)
	conn, err := tls.Dial("tcp", smtpHostWithPort, tlsconfig)
	if err != nil {
		log.Panic(err)
	}

	c, err := smtp.NewClient(conn, smtpHost)
	if err != nil {
		log.Panic(err)
	}

	// Auth
	if err = c.Auth(auth); err != nil {
		log.Panic(err)
	}

	// To && From
	if err = c.Mail(from.Address); err != nil {
		log.Panic(err)
	}

	if err = c.Rcpt(to.Address); err != nil {
		log.Panic(err)
	}

	// Data
	w, err := c.Data()
	if err != nil {
		log.Panic(err)
	}

	_, err = w.Write([]byte(message))
	if err != nil {
		log.Panic(err)
	}

	err = w.Close()
	if err != nil {
		log.Panic(err)
	}

	c.Quit()

	fmt.Println("Email Sent Successfully!")
}
