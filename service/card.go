package service

import "online-tabu-server/models"

func FindCard(cardId string) (*models.Card, error) {

	var card models.Card
	err := models.DB.Where("id = ?", cardId).First(&card).Error
	if err != nil {
		return nil, err
	}
	return &card, nil
}
