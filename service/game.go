package service

import (
	uuid "github.com/satori/go.uuid"
	"math/rand"
	"online-tabu-server/models"
	"online-tabu-server/ssehandler"
	"sort"
	"strconv"
	"time"
)

var TickStream chan int
var CountDown int

func CreateGame(stage *models.Stage, input *models.CreateGameInput, gameRoundCount int) (*models.Game, error) {

	var game models.Game
	game.Id = uuid.NewV4().String()
	game.CreatedAt = time.Now()
	game.ValidUntil = time.Now().Add(12 * time.Hour)
	game.TimeToGuessSeconds = input.TimeToGuessSeconds
	game.CurrentRoundIndex = 0
	game.StageID = stage.Id
	game.Finished = false
	game.MaxRounds = gameRoundCount

	models.DB.Create(&game)

	var index int
	teamCount := len(stage.Teams)

	for _, team := range stage.Teams {
		result := models.Result{
			Id:                   uuid.NewV4().String(),
			GameID:               game.Id,
			TeamID:               team.Id,
			CardsWon:             0,
			AverageRoundDuration: 0,
		}
		models.DB.Create(&result)
		game.Results = append(game.Results, result)
	}

	var cardPool []models.Card
	models.DB.Find(&cardPool)

	var gameRounds []models.Round

	teamPlayerCounterMap := make(map[string]int)
	teamMonitorCounterMap := make(map[string]int)

	for index = 0; index < gameRoundCount; index++ {
		playingTeamIndex := index % teamCount
		playingTeam := stage.Teams[playingTeamIndex]

		teamPlayerCounterMap[playingTeam.Id] += 1
		teamPlayerIndex := teamPlayerCounterMap[playingTeam.Id] % len(playingTeam.Players)
		teamReaderIndex := (teamPlayerCounterMap[playingTeam.Id] + 1) % len(playingTeam.Players)

		guessingPlayer := playingTeam.Players[teamPlayerIndex]
		readingPlayer := playingTeam.Players[teamReaderIndex]

		monitoringTeamIndex := (index + 1) % teamCount
		monitoringTeam := stage.Teams[monitoringTeamIndex]

		teamMonitorCounterMap[monitoringTeam.Id] += 1
		teamMonitoringIndex := teamMonitorCounterMap[monitoringTeam.Id] % len(monitoringTeam.Players)
		monitoringPlayer := monitoringTeam.Players[teamMonitoringIndex]

		card := findRandomCard(gameRounds, cardPool)
		round, _ := CreateRound(&game,
			index,
			readingPlayer.Id,
			guessingPlayer.Id,
			monitoringPlayer.Id,
			card)

		gameRounds = append(gameRounds, *round)
	}

	game.Rounds = gameRounds

	sort.Slice(game.Rounds, func(i, j int) bool {
		return game.Rounds[i].OrderId < game.Rounds[j].OrderId
	})

	resetTimer(&game)

	return &game, nil
}

func findRandomCard(rounds []models.Round, cardPool []models.Card) *models.Card {

	rand.Seed(time.Now().Unix())

	randIndex := rand.Intn(len(cardPool))

	for gameHasCardAlready(rounds, &cardPool[randIndex]) {
		randIndex = rand.Intn(len(cardPool))
	}

	return &cardPool[randIndex]
}

func gameHasCardAlready(rounds []models.Round, card *models.Card) bool {
	for _, round := range rounds {
		if round.CardID == card.Id {
			return true
		}
	}
	return false
}

func FindGame(gameId string) (*models.Game, error) {
	var game models.Game
	models.DB.Preload("Results").Where("id = ?", gameId).First(&game)

	var rounds []models.Round
	models.DB.Where("game_id = ?", gameId).Order("order_id ASC").Find(&rounds)

	game.Rounds = rounds

	return &game, nil
}

func GameNextRound(gameId string) {
	var game models.Game
	models.DB.Where("id = ?", gameId).First(&game)
	if (game.CurrentRoundIndex + 1) < game.MaxRounds {
		resetTimer(&game)
		game.CurrentRoundIndex = game.CurrentRoundIndex + 1
	} else {
		game.Finished = true
		CountDown = 0
	}

	models.DB.Save(&game)
}

func resetTimer(game *models.Game) {

	if CountDown > 0 {
		CountDown = game.TimeToGuessSeconds
	} else {
		CountDown = game.TimeToGuessSeconds
		go func() {

			for CountDown > 0 {

				ssehandler.EHandler.SendString(game.StageID, "timer."+strconv.Itoa(CountDown))
				CountDown -= 1
				time.Sleep(time.Second)
			}
			ssehandler.EHandler.SendString(game.StageID, "timer.finished")
		}()
	}

}

func StartTickStream() {

	//currentRound,_ := FindRound(game.Rounds[game.CurrentRoundIndex].Id);
	//if(!currentRound.Answered){
	//AnswerRound(&models.UpdateRound{false,true},currentRound)
	//}

}
