package service

import (
	uuid "github.com/satori/go.uuid"
	"online-tabu-server/models"
)

func CreateTeam(stage *models.Stage, name string) (*models.Team, error) {
	team := models.Team{Id: uuid.NewV4().String(), StageID: stage.Id, Name: name}

	err := models.DB.Create(&team).Error
	if err != nil {
		return nil, err
	}

	return &team, nil
}

func FindTeam(id string) (*models.Team, error) {
	var team models.Team

	err := models.DB.Where("id = ?", id).First(&team).Error

	if err != nil {
		return nil, err
	}

	return &team, nil
}
