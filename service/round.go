package service

import (
	"errors"
	uuid "github.com/satori/go.uuid"
	"online-tabu-server/models"
)

func CreateRound(game *models.Game, index int,
	readingPlayerId string,
	guessingPlayerId string,
	monitoringPlayerId string,
	card *models.Card) (*models.Round, error) {

	round := models.Round{
		Id:                 uuid.NewV4().String(),
		OrderId:            index,
		ReadingPlayerID:    readingPlayerId,
		GuessingPlayerID:   guessingPlayerId,
		MonitoringPlayerID: monitoringPlayerId,
		RoundWon:           false,
		RoundDuration:      game.TimeToGuessSeconds,
		GameID:             game.Id,
		CardID:             card.Id,
	}

	models.DB.Create(&round)

	return &round, nil
}

func FindRound(roundId string) (*models.Round, error) {

	var round models.Round
	err := models.DB.Where("id = ?", roundId).First(&round).Error
	if err != nil {
		return nil, err
	}

	return &round, nil
}

func AnswerRound(input *models.UpdateRound, round *models.Round) (*models.Round, error) {
	if round.Answered {
		return nil, errors.New("Runde bereits beantwortet")
	}
	round.Answered = input.Answered
	round.RoundWon = input.RoundWon

	err := models.DB.Save(&round).Error
	if err != nil {
		return nil, err
	}

	game, _ := FindGame(round.GameID)
	player, _ := FindPlayer(round.GuessingPlayerID)
	if round.RoundWon {
		for _, r := range game.Results {
			if r.TeamID == player.TeamID {
				r.CardsWon += 1
				models.DB.Save(&r)
				break
			}
		}
	}

	GameNextRound(round.GameID)
	return round, nil
}
