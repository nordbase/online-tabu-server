package service

import (
	"fmt"
	uuid "github.com/satori/go.uuid"
	"online-tabu-server/models"
)

func CreatePlayer(t *models.Team, input *models.CreatePlayerInput) (*models.Player, error) {
	player := models.Player{
		Id:        uuid.NewV4().String(),
		Name:      input.Name,
		Email:     input.Email,
		TeamID:    t.Id,
		LoginCode: input.LoginCode,
	}
	err := models.DB.Create(&player).Error

	if err != nil {
		return nil, err
	}
	if "" != player.Email {
		fmt.Println("Sending invitation to " + player.Email)
		SendInvitationEmail(t.StageID, &player)
	}

	return &player, nil
}

func DeletePlayer(playerId string) error {
	return models.DB.Where(" id = ?", playerId).Delete(models.Player{}).Error
}

func FindPlayer(id string) (*models.Player, error) {
	var player models.Player

	err := models.DB.Where("id = ?", id).First(&player).Error

	if err != nil {
		return nil, err
	}

	return &player, nil
}

func FindPlayerByLoginCode(loginCode string) (*models.Player, error) {
	var player models.Player

	err := models.DB.Where("login_code = ?", loginCode).First(&player).Error

	if err != nil {
		return nil, err
	}

	return &player, nil
}
