package service

import (
	uuid "github.com/satori/go.uuid"
	"online-tabu-server/models"
)

func CreateStage(input *models.CreateStageInput) (*models.Stage, error) {

	stage := models.Stage{Id: uuid.NewV4().String()}
	err := models.DB.Create(&stage).Error
	if err != nil {
		return nil, err
	}

	teamRed, err := CreateTeam(&stage, "Blau")
	if err != nil {
		return nil, err
	}

	createPlayerInput := models.CreatePlayerInput{Name: input.FirstPlayerName}
	player, err := CreatePlayer(teamRed, &createPlayerInput)
	if err != nil {
		return nil, err
	}
	stage.FirstPlayerID = player.Id
	teamRed.Players = append(teamRed.Players, player)
	stage.Teams = append(stage.Teams, *teamRed)

	teamGreen, err := CreateTeam(&stage, "Grün")
	if err != nil {
		return nil, err
	}

	stage.Teams = append(stage.Teams, *teamGreen)

	err = models.DB.Save(&stage).Error

	if err != nil {
		return nil, err
	}

	return &stage, nil
}

func FindStage(stageId string) (*models.Stage, error) {
	var stage models.Stage
	err := models.DB.Preload("Teams.Players").Preload("Games").Where("id = ?", stageId).First(&stage).Error
	return &stage, err
}
