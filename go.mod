module online-tabu-server

go 1.15

require (
	github.com/gin-gonic/gin v1.6.3
	github.com/jinzhu/gorm v1.9.16
	github.com/mattn/go-sqlite3 v1.14.5 // indirect
	github.com/satori/go.uuid v1.2.0
	gorm.io/driver/mysql v1.0.4
	gorm.io/gorm v1.20.12
)
