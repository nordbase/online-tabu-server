package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"online-tabu-server/auth"
	"online-tabu-server/config"
	"online-tabu-server/controller"
	"online-tabu-server/models"
	"online-tabu-server/ssehandler"
)

func main() {

	fmt.Printf("Hello %s\n", "welt")
	config.Init()
	models.InitDB()

	ehandler := ssehandler.SetupEHandler()
	ehandler.HandleEvents()

	engine := initServer()
	engine.Run()

}

func initServer() *gin.Engine {

	engine := gin.Default()
	engine.GET("/status", func(c *gin.Context) {

		c.JSON(http.StatusOK, gin.H{"status": "RUNNING"})
	})

	engine.Use(CORSMiddleware())

	api := engine.Group("/api").Use(auth.Authorize())
	{
		api.GET("/stages/:stageId/eventhub", ssehandler.EHandler.Subscribe)

		api.POST("/stages", controller.CreateStage)
		api.GET("/stages/:stageId", controller.FindStage)
		api.DELETE("/stages/:stageId", controller.DeleteStage)

		api.GET("/players/:playerId", controller.FindPlayer)
		api.GET("/players/", controller.FindPlayerByLoginCode)
		api.POST("/stages/:stageId/teams/:teamId/players", controller.CreatePlayer)
		//api.GET("/stages/:stageId/teams/:teamId/players", controller.FindAllGames)
		//api.GET("/stages/:stageId/teams/:teamId/players/:playerId", controller.FindGame)
		api.DELETE("/stages/:stageId/teams/:teamId/players/:playerId", controller.DeletePlayer)

		api.POST("/stages/:stageId/games", controller.CreateGame)
		api.GET("/stages/:stageId/games", controller.FindAllGames)
		api.GET("/stages/:stageId/games/:gameId", controller.FindGame)
		api.PUT("/stages/:stageId/games/:gameId", controller.UpdateGame)
		api.DELETE("/stages/:stageId/games/:gameId", controller.DeleteGame)

		api.GET("/stages/:stageId/games/:gameId/rounds", controller.FindAllGames)
		api.GET("/stages/:stageId/games/:gameId/rounds/:roundId", controller.FindGame)
		api.PATCH("/stages/:stageId/games/:gameId/rounds/:roundId", controller.UpdateRound)
	}

	return engine
}

func CORSMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {

		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Credentials", "true")
		c.Header("Access-Control-Allow-Headers", "Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
		c.Header("Access-Control-Allow-Methods", "POST,HEAD,PATCH,DELETE, OPTIONS, GET, PUT")

		if c.Request.Method == "OPTIONS" {
			c.AbortWithStatus(204)
			return
		}

		c.Next()
	}
}
