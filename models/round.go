package models

type UpdateRound struct {
	RoundWon bool `json:"roundWon"`
	Answered bool `json:"answered"`
}

type Round struct {
	HalModel
	Id                 string `json:"id" gorm:"primary_key"`
	OrderId            int    `json:"orderId"`
	ReadingPlayerID    string `json:"readingPlayerId"`
	GuessingPlayerID   string `json:"guessingPlayerId"`
	MonitoringPlayerID string `json:"monitoringPlayerId"`
	RoundWon           bool   `json:"roundWon"`
	Answered           bool   `json:"answered"`
	RoundDuration      int    `json:"round_duration"`
	GameID             string `json:"game_id" binding:"required" gorm:"size:40"`
	CardID             string `json:"card_id" binding:"required" gorm:"size:40"`
}
