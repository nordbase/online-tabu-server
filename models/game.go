package models

import (
	"time"
)

type CreateGameInput struct {
	TimeToGuessSeconds int `json:"timeToGuessSeconds"`
}

type Game struct {
	HalModel
	Id                 string    `json:"id" gorm:"primary_key"`
	StageID            string    `json:"stageId" binding:"required" gorm:"size:50"`
	CreatedAt          time.Time `json:"createdAt"`
	ValidUntil         time.Time `json:"validUntil"`
	TimeToGuessSeconds int       `json:"timeToGuessSeconds"`
	Rounds             []Round   `json:"-"  gorm:"foreignKey:GameID"`
	Results            []Result  `json:"results,omitempty"  gorm:"foreignKey:GameID"`
	CurrentRoundIndex  int       `json:"currentRoundIndex"`
	MaxRounds          int       `json:"maxRounds"`
	Finished           bool      `json:"finished"`
}
