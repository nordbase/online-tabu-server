package models

type CreatePlayerInput struct {
	Name      string `json:"name" binding:"required"`
	Email     string `json:"email"`
	LoginCode string `json:"loginCode"`
}

type Player struct {
	HalModel
	Id        string `json:"id" gorm:"primary_key"`
	Name      string `json:"name" binding:"required"`
	Email     string `json:"email" binding:"required"`
	Status    string `json:"status" gorm:"-"` // z.B. eingeladen oder online oder offline
	TeamID    string `json:"teamId" binding:"required" gorm:"size:40"`
	StageID   string `json:"stageId" gorm:"-"`
	LoginCode string `json:"loginCode"`
}
