package models

import (
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"log"
	"online-tabu-server/config"
)

var DB *gorm.DB

func InitDB() {

	dbuser := config.DBUser
	dbpassword := config.DBPassword
	dbserver := config.DBServer

	//database, err := gorm.Open(sqlite.Open("gorm.db"), &gorm.Config{})
	//database, err := gorm.Open("mysql", dbuser+":"+dbpassword+"@tcp("+dbserver+")/online-tabu?charset=utf8&parseTime=True")

	dsn := dbuser + ":" + dbpassword + "@tcp(" + dbserver + ")/online-tabu?charset=utf8mb4&parseTime=True"
	database, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		log.Println("Connection Failed to Open")
	} else {
		log.Println("Connection Established")
	}

	database.AutoMigrate(&Stage{}, &Game{}, &Team{}, &Player{}, &Round{}, &Result{}, &Card{})

	DB = database

}
