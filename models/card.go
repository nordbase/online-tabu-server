package models

type Card struct {
	Id             string `json:"id"`
	SecretWord     string `json:"secret_word"`
	ForbiddenWord1 string `json:"forbidden_word_1"`
	ForbiddenWord2 string `json:"forbidden_word_2"`
	ForbiddenWord3 string `json:"forbidden_word_3"`
	ForbiddenWord4 string `json:"forbidden_word_4"`
	ForbiddenWord5 string `json:"forbidden_word_5"`
}
