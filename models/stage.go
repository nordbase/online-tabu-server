package models

type Stage struct {
	HalModel
	Id            string `json:"id"" gorm:"primaryKey"`
	Teams         []Team `json:"teams,omitempty" `
	FirstPlayerID string `json:"firstPlayerId" `
	Games         []Game `json:"games,omitempty" gorm:"foreignKey:StageID"`
}

type CreateStageInput struct {
	FirstPlayerName string `json:"firstPlayerName"`
}

//func (s *Stage) Players()  []Player{
//	return append(s.Teams[0].Players,s.Teams[1].Players...)
//}
