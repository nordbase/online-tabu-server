package models

type Result struct {
	Id                   string `json:"id" gorm:"primary_key" gorm:"size:40"`
	GameID               string `json:"gameId" gorm:"size:40"`
	TeamID               string `json:"teamId" gorm:"size:40"`
	CardsWon             int    `json:"cardsWon"`
	AverageRoundDuration uint   `json:"average_round_duration"`
}
