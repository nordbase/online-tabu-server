package models

type Team struct {
	Id      string    `json:"id" gorm:"primary_key"`
	Name    string    `json:"name"`
	StageID string    `json:"stage_id" binding:"required" gorm:"size:40"`
	Players []*Player `json:"players"`
}
