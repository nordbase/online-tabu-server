package models

type HalModel struct {
	Embedded map[string]interface{} `json:"_embedded,omitempty" gorm:"-"`
	Links    map[string]string      `json:"_links,omitempty" gorm:"-"`
}
