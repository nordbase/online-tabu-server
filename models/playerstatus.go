package models

type PlayerStatus interface {
	Id() uint
	Name() string
}

type PLayerStatusInvited struct {
}

func (i PLayerStatusInvited) Name() string {
	return "INVITED"
}
func (i PLayerStatusInvited) Id() uint {
	return 0
}

type PLayerStatusOnline struct {
}

func (i PLayerStatusOnline) Name() string {
	return "ONLINE"
}
func (i PLayerStatusOnline) Id() uint {
	return 1
}

type PLayerStatusOffline struct {
}

func (i PLayerStatusOffline) Name() string {
	return "OFFLINE"
}
func (i PLayerStatusOffline) Id() uint {
	return 0
}
