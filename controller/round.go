package controller

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"online-tabu-server/config"
	"online-tabu-server/models"
	"online-tabu-server/service"
	"online-tabu-server/ssehandler"
)

func UpdateRound(c *gin.Context) {
	stageId := c.Param("stageId")
	roundId := c.Param("roundId")

	round, err := service.FindRound(roundId)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotFound, err)
		return
	}
	if !round.Answered {
		var input models.UpdateRound
		if err := c.ShouldBindJSON(&input); err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}

		round, err = service.AnswerRound(&input, round)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
			return
		}
	}

	ssehandler.EHandler.SendString(stageId, "round.updated")
	c.JSON(http.StatusOK, round)
}

func buildRound(stageId string, gameId string, round *models.Round) *models.Round {
	round.Embedded = make(map[string]interface{})
	round.Embedded["readingPlayer"], _ = service.FindPlayer(round.ReadingPlayerID)
	round.Embedded["guessingPlayer"], _ = service.FindPlayer(round.GuessingPlayerID)
	round.Embedded["monitoringPlayer"], _ = service.FindPlayer(round.MonitoringPlayerID)
	round.Embedded["card"], _ = service.FindCard(round.CardID)
	round.Links = make(map[string]string)
	round.Links["self"] = config.BaseUrl + "/api/stages/" + stageId + "/games/" + gameId + "/rounds/" + round.Id

	return round
}
