package controller

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"online-tabu-server/config"
	"online-tabu-server/models"
	"online-tabu-server/service"
	"online-tabu-server/ssehandler"
)

func CreatePlayer(c *gin.Context) {
	teamId := c.Param("teamId")
	team, err := service.FindTeam(teamId)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": err.Error()})
	}

	var input models.CreatePlayerInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	player, err := service.CreatePlayer(team, &input)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}
	buildPlayer(team.StageID, team.Id, player)
	ssehandler.EHandler.SendString(team.StageID, "player.created")
	c.JSON(http.StatusCreated, player)
}

func FindPlayer(c *gin.Context) {

	playerId := c.Param("playerId")
	player, err := service.FindPlayer(playerId)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}
	team, err := service.FindTeam(player.TeamID)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}

	buildPlayer(team.StageID, team.Id, player)
	c.JSON(http.StatusOK, player)
}

func DeletePlayer(c *gin.Context) {
	stageId := c.Param("stageId")
	playerId := c.Param("playerId")
	err := service.DeletePlayer(playerId)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}
	ssehandler.EHandler.SendString(stageId, "player.deleted")
	c.JSON(http.StatusNoContent, nil)
}

func buildPlayer(stageId string, teamId string, player *models.Player) *models.Player {
	if ssehandler.EHandler.IsClientOnline(player.Id) {
		player.Status = "ONLINE"
	} else {
		player.Status = "OFFLINE"
	}
	player.StageID = stageId
	player.Embedded = make(map[string]interface{})
	player.Links = make(map[string]string)
	player.Links["self"] = config.BaseUrl + "/api/stages/" + stageId + "/teams/" + teamId + "/players/" + player.Id
	return player
}
