package controller

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"online-tabu-server/config"
	"online-tabu-server/models"
	"online-tabu-server/service"
)

func CreateStage(c *gin.Context) {
	var input models.CreateStageInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	stage, err := service.CreateStage(&input)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, err)
		return
	}

	buildStage(stage)

	c.JSON(http.StatusCreated, stage)
}

func FindStage(c *gin.Context) {
	stageId := c.Param("stageId")
	stage, err := service.FindStage(stageId)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	buildStage(stage)
	c.JSON(http.StatusOK, stage)
}

func DeleteStage(c *gin.Context) {

}

func FindPlayerByLoginCode(c *gin.Context) {

	loginCode := c.Query("loginCode")
	player, err := service.FindPlayerByLoginCode(loginCode)
	if err != nil || player == nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	team, err := service.FindTeam(player.TeamID)
	if err != nil || player == nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	buildPlayer(team.StageID, team.Id, player)
	c.JSON(http.StatusOK, player)
}

func buildStage(stage *models.Stage) *models.Stage {

	stage.Embedded = make(map[string]interface{})
	stage.Embedded["firstPlayer"], _ = service.FindPlayer(stage.FirstPlayerID)
	stage.Links = make(map[string]string)
	stage.Links["self"] = config.BaseUrl + "/api/stages/" + stage.Id

	for _, t := range stage.Teams {
		for _, p := range t.Players {
			p = buildPlayer(t.StageID, t.Id, p)
		}
	}

	return stage
}
