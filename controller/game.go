package controller

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"online-tabu-server/config"
	"online-tabu-server/models"
	"online-tabu-server/service"
	"online-tabu-server/ssehandler"
)

func CreateGame(c *gin.Context) {
	stageId := c.Param("stageId")
	stage, err := service.FindStage(stageId)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": err.Error()})
	}

	var input models.CreateGameInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	game, err := service.CreateGame(stage, &input, config.DefaultGameRounds)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}
	buildGame(stage, game)
	ssehandler.EHandler.SendString(stage.Id, "game.created")
	c.JSON(http.StatusCreated, game)
}

func FindGame(c *gin.Context) {
	stageId := c.Param("stageId")
	stage, err := service.FindStage(stageId)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": err.Error()})
	}

	gameId := c.Param("gameId")
	game, err := service.FindGame(gameId)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}
	buildGame(stage, game)
	c.JSON(http.StatusOK, game)
}
func FindAllGames(c *gin.Context) {

	var games []models.Game

	models.DB.Preload("Teams.Players").Find(&games)

	c.JSON(http.StatusOK, games)
}
func DeleteGame(c *gin.Context) {

}
func UpdateGame(c *gin.Context) {

}

func buildGame(stage *models.Stage, game *models.Game) {
	game.Embedded = make(map[string]interface{})
	game.Embedded["currentRound"] = buildRound(game.StageID, game.Id, &game.Rounds[game.CurrentRoundIndex])
	game.Embedded["stage"] = buildStage(stage)
	game.Links = make(map[string]string)
	game.Links["self"] = config.BaseUrl + "/api/stages/" + game.StageID + "/games/" + game.Id
}
