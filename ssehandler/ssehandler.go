// Server-side events handler for Gin.
// Based on work by Kyle L. Jensen
// Source: https://github.com/kljensen/golang-html5-sse-example

package ssehandler

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
)

var EHandler *SSEHandler

func SetupEHandler() *SSEHandler {
	EHandler = NewSSEHandler()
	return EHandler

}

type SSEClient struct {
	StageID  string
	ClientID string
	Channel  chan string
}

type SSEMessage struct {
	StageID string
	Body    string
}

type SSEHandler struct {
	// Create a map of clients, the keys of the map are the channels over
	// which we can push messages to attached clients. (The values are just
	// booleans and are meaningless.)
	clients map[chan string]SSEClient

	// Channel into which new clients can be pushed
	newClients chan SSEClient

	// Channel into which disconnected clients should be pushed
	defunctClients chan chan string

	// Channel into which messages are pushed to be broadcast out
	messages chan SSEMessage
}

// Make a new SSEHandler instance.
func NewSSEHandler() *SSEHandler {
	b := &SSEHandler{
		clients:        make(map[chan string]SSEClient),
		newClients:     make(chan SSEClient),
		defunctClients: make(chan (chan string)),
		messages:       make(chan SSEMessage, 10), // buffer 10 msgs and don't block sends
	}
	return b
}

// Start handling new and disconnected clients, as well as sending messages to
// all connected clients.
func (b *SSEHandler) HandleEvents() {
	go func() {
		for {
			select {
			case s := <-b.newClients:
				fmt.Printf("adding client %v ", s)
				b.clients[s.Channel] = s
				EHandler.SendString(s.StageID, "player.joined")
			case s := <-b.defunctClients:
				fmt.Printf("removing client %v ", s)
				delete(b.clients, s)
				close(s)
			case msg := <-b.messages:
				fmt.Printf("Broadcasting message\n")
				for channel, client := range b.clients {
					if msg.StageID == client.StageID {
						fmt.Printf("Broadcasting message to stage %v\n", msg.StageID)
						channel <- msg.Body
					}

				}
			}
		}
	}()
}

// Send out a simple string to all clients.
func (b *SSEHandler) SendString(stageId string, msg string) {
	b.messages <- SSEMessage{StageID: stageId, Body: msg}
}

func (b *SSEHandler) IsClientOnline(playerToken string) bool {
	for _, v := range b.clients {
		if playerToken == v.ClientID {
			return true
		}
	}
	return false
}

// Send out a JSON string object to all clients.
func (b *SSEHandler) SendJSON(stageId string, obj interface{}) {
	tmp, err := json.Marshal(obj)
	if err != nil {
		log.Panic("Error while sending JSON object:", err)
	}
	b.messages <- SSEMessage{StageID: stageId, Body: string(tmp)}
}

// Subscribe a new client and start sending out messages to it.
func (b *SSEHandler) Subscribe(c *gin.Context) {
	stageId := c.Param("stageId")
	w := c.Writer
	f, ok := w.(http.Flusher)
	if !ok {
		c.AbortWithError(http.StatusBadRequest, fmt.Errorf("Streaming unsupported"))
		return
	}

	playerToken := c.Query("playerToken")
	fmt.Printf("subscribing with client id %v\n‚", playerToken)
	// Create a new channel, over which we can send this client messages.
	messageChan := make(chan string)
	// Add this client to the map of those that should receive updates
	b.newClients <- SSEClient{StageID: stageId, ClientID: playerToken, Channel: messageChan}

	//notify := w.(http.CloseNotifier).CloseNotify()
	notify := c.Request.Context().Done()
	go func() {
		<-notify
		// Remove this client from the map of attached clients
		b.defunctClients <- messageChan
	}()

	w.Header().Set("Content-Type", "text/event-stream")
	w.Header().Set("Cache-Control", "no-cache")
	w.Header().Set("Connection", "keep-alive")

	for {
		msg, open := <-messageChan
		if !open {
			// If our messageChan was closed, this means that
			// the client has disconnected.
			break
		}

		fmt.Fprintf(w, "data: %s\n\n", msg)
		// Flush the response. This is only possible if the repsonse
		// supports streaming.
		f.Flush()
	}

	c.AbortWithStatus(http.StatusOK)
}
