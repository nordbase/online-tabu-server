package config

import "os"

var JwtPublicKey string

var DBServer string
var DBUser string
var DBPassword string

var ClientID string
var ClientSecret string

var BaseUrl string
var AppBaseUrl string

var SmtpServer string
var SmtpPort string
var SmtpUser string
var SmtpPassword string

const DefaultGameRounds = 10

func Init() {

	BaseUrl = os.Getenv("SERVER_BASE_URL")
	AppBaseUrl = os.Getenv("APP_BASE_URL")

	DBServer = os.Getenv("DB_SERVER")
	DBUser = os.Getenv("DB_USER")
	DBPassword = os.Getenv("DB_PASSWORD")

	ClientID = os.Getenv("CLIENT_ID")
	ClientSecret = os.Getenv("CLIENT_SECRET")

	SmtpServer = os.Getenv("SMTP_SERVER")
	SmtpPort = os.Getenv("SMTP_PORT")
	SmtpUser = os.Getenv("SMTP_USER")
	SmtpPassword = os.Getenv("SMTP_PASSWORD")

}
