package auth

import (
	"fmt"
	"github.com/gin-gonic/gin"
)

func Authorize() gin.HandlerFunc {
	test := func(c *gin.Context) {
		//c.Next()
		authHeader := c.GetHeader("Authorization")
		fmt.Println("Auth Header:" + authHeader)
		c.Set("USERID", authHeader)
	}

	return test
}
